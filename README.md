# 読取ページ画像取得バッチ

## build

#### 本番
    clean install -Dmaven.test.skip=true package -P production

## 設置場所

/opt/ConsoleWeb/batch/ 以下に シェルスクリプトおよびjarを設置する

※ PageImageDL-jar-with-dependencies.jar の方を設置してください

## ページ画像の取得

- java製です

- env.properties の download.path に保存先を指定する

- env.properties の execute.limit.minute に実行時間制限を指定する

    (指定しなければ全ReadingUnitのページを取得したら終了する)

- PageImageDownloadsStart.sh から実行できる

- /opt/ConsoleWeb/batch/ に、最後に取得した読取ユニットのIDをメモしたファイル(last_id_memo.txt)が設置されています

    次回起動時に続きから動作する為に必要です。
    重複防止の為、消さないでください

- cron例: 15分毎に起動

        */15 * * * * /bin/bash /opt/ConsoleWeb/batch/PageImageDownloadStart.sh

## 取得したページ画像の削除

- シェルスクリプト単体です

- PageImageDelete.sh の IMAGE_DIR_PATH に画像の保存先を指定する

- PageImageDelete.sh の DELETE_DAY に削除対象を何日前より古いファイルか指定する

- PageImageDelete.sh を実行する

- cron例: 毎日2時に起動

        * 2 * * * /bin/bash /opt/ConsoleWeb/batch/PageImageDelete.sh