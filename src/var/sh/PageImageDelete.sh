#!bin/bash

#############################################################
# 設定

# 画像の保存先ディレクトリ
IMAGE_DIR_PATH=/tmp/test

# 何日前まで削除するか
DELETE_DAY=60

#############################################################
# 以下、処理

# 対象ディレクトリへ移動
cd $IMAGE_DIR_PATH

# DELETE_TIMEより古い.pngのファイルを削除する
find ./ -name "*.png" -mtime +$DELETE_DAY -type f -exec rm -f {} \;
# DELETE_TIMEより古い.jpgのファイルを削除する
find ./ -name "*.jpg" -mtime +$DELETE_DAY -type f -exec rm -f {} \;

# 空のディレクトリ削除 (中にファイルがある場合は削除されない)
# man: The rmdir utility removes the directory entry specified by each directory argument, provided it is empty.
# 翻訳: rmdirユーティリティーは、各ディレクトリー引数で指定されたディレクトリー項目が空である場合は除去します。
rmdir * > /tmp/PageImageDelete.log 2&>1
