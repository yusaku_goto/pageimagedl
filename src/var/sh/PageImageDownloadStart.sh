#!bin/bash

# set java path
JAVA_PATH=/usr/bin/java

cd /opt/ConsoleWeb/batch/

sudo -u glassfish ${JAVA_PATH} -jar -Dfile.encoding=UTF-8 -Duser.timezone=Asia/Tokyo -Xmx3g -Xms3g PageImageDL-jar-with-dependencies.jar &
