package ai.inside.common.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * このクラスはConnectionオブジェクトの呼び出しにインジェクトするためのハンドラーです。
 * @author yuukiitonaga
 *
 */
public class ConnectionProxyHandler implements InvocationHandler {
	private final Connection con;
	ConnectionProxyHandler(Connection con) {
		this.con = con;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// PreparedStatementのProxy化をします。
		if (method.getName().equals("prepareStatement")) {
			PreparedStatement ps = (PreparedStatement) method.invoke(con, args);
			return Proxy.newProxyInstance(
					PreparedStatement.class.getClassLoader(),
					new Class[]{PreparedStatement.class},
					new PreparedStatementProxyHandler(ps, (String) args[0]));
		} else {
			return method.invoke(con, args);
		}
	}
}
