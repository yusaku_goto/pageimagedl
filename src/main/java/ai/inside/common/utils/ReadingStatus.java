package ai.inside.common.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * ReadingUnit/ReadingPage/ReadingPartsのstatusの意味を保持するクラスです。
 */
public class ReadingStatus {
	/** 1:ユニットのみ登録された状態 */
	public static final int UNIT_NEW = 1; // ユニットのみ登録された状態
	/** 2:ページの追加処理中 */
	public static final int IN_PAGE_CREATE = 2; // ページの追加処理中
	/** 3:ページの追加完了 */
	public static final int CMP_PAGE_CREATE = 3; // ページの追加完了
	/** 4:仕分け処理中 */
	public static final int IN_TRAITS = 4; // 仕分け処理中
	/** 5:仕分けエラー */
	public static final int ERR_TRAITS = 5; // 仕分けエラー
	/** 6:仕分け完了 */
	public static final int CMP_TRAITS = 6; // 仕分け完了
	/** 7:パーツ生成エラー */
	public static final int ERR_PARTS_CREATE = 7; // パーツ生成エラー
	/** 8:パーツ生成中 */
	public static final int IN_PARTS_CREATE = 8; // パーツ生成中
	/** 9:パーツ生成完了 */
	public static final int CMP_PARTS_CREATE = 9; // パーツ生成完了
	/** 10:NX読取エラー */
	public static final int ERR_NX_READ = 10; // NX読取エラー
	/** 11:NX読取中（読取のためのパーツDLをした場合を含む） */
	public static final int IN_NX_READ = 11; // NX読取中（読取のためのパーツDLをした場合を含む）
	/** 12:NX読取完了（resultがセットされた状態） */
	public static final int CMP_NX_READ = 12; // NX読取完了（resultがセットされた状態）
	/** 13:エントリー中 */
	public static final int IN_VERIFY = 13; // エントリー中
	/** 14:エントリーエラー */
	public static final int ERR_VERIFY = 14; // エントリーエラー
	/** 15:エントリー完了（仮入力） */
	public static final int FIN_VERIFY = 15; // エントリー完了（仮入力）
	/** 115:ベリファイ処理中 */
	public static final int IN_VERIFY2 = 115; // ベリファイ中
	/** 16:ベリファイ完了（承認済） */
	public static final int CMP_VERIFY = 16; // ベリファイ完了（承認済）
	/** 17:加工処理エラー */
	public static final int ERR_MANIPULATION = 17; // 加工処理エラー
	/** 18:加工処理中 */
	public static final int IN_MANIPULATION = 18; // 加工処理中
	/** 19:加工処理完了 */
	public static final int CMP_MANIPULATION = 19; // 加工処理完了
	/** 20:CSV出力エラー */
	public static final int ERR_CSV_WRITE = 20; // CSV出力エラー
	/** 21:CSV出力中 */
	public static final int IN_CSV_WRITE = 21; // CSV出力中
	/** 22:CSV出力完了 */
	public static final int CMP_CSV_WRITE = 22; // CSV出力完了

	private static Map<Integer, String> pageMap = new HashMap<>();
	static {
		pageMap.put(UNIT_NEW, "新規登録");
		pageMap.put(IN_PAGE_CREATE, "ページ登録中");
		pageMap.put(CMP_PAGE_CREATE, "ページ登録済");
		pageMap.put(IN_TRAITS, "仕分け処理中");
		pageMap.put(ERR_TRAITS, "仕分けエラー");
		pageMap.put(CMP_TRAITS, "仕分け完了");
		pageMap.put(IN_PARTS_CREATE, "パーツ化処理中");
		pageMap.put(ERR_PARTS_CREATE, "パーツ化エラー");
		pageMap.put(CMP_PARTS_CREATE, "パーツ化完了");
		pageMap.put(IN_NX_READ, "読取処理中");
		pageMap.put(ERR_NX_READ, "読取エラー");
		pageMap.put(CMP_NX_READ, "読取完了");
		pageMap.put(IN_VERIFY, "エントリー処理中");
		pageMap.put(ERR_VERIFY, "エントリーエラー");
		pageMap.put(FIN_VERIFY, "エントリー完了");
		pageMap.put(IN_VERIFY2, "ベリファイ処理中");
		pageMap.put(CMP_VERIFY, "ベリファイ完了");
		pageMap.put(IN_MANIPULATION, "加工処理中");
		pageMap.put(ERR_MANIPULATION, "加工エラー");
		pageMap.put(CMP_MANIPULATION, "加工完了");
		pageMap.put(IN_CSV_WRITE, "CSV出力処理中");
		pageMap.put(ERR_CSV_WRITE, "CSV出力エラー");
		pageMap.put(CMP_CSV_WRITE, "CSV出力完了");
	}

	/**
	 * 状態を現在のシステムが順序付けられた番号で返します。
	 * この番号を比較すると、値が小さいほうが前処理になります。
	 * @param status
	 * @return
	 */
	public static int getOrderedStatus(int status) {
		int i = 0;
		// ステータスが逆順に列挙されているので注意
		switch (status) {
		case CMP_CSV_WRITE:
			i++;
		case IN_CSV_WRITE:
			i++;
		case ERR_CSV_WRITE:
			i++;
		case CMP_MANIPULATION:
			i++;
		case IN_MANIPULATION:
			i++;
		case ERR_MANIPULATION:
			i++;
		case CMP_VERIFY:
			i++;
		case IN_VERIFY2:
			i++;
		case FIN_VERIFY:
			i++;
		case ERR_VERIFY:
			i++;
		case IN_VERIFY:
			i++;
		case CMP_NX_READ:
			i++;
		case IN_NX_READ:
			i++;
		case ERR_NX_READ:
			i++;
		case CMP_PARTS_CREATE:
			i++;
		case IN_PARTS_CREATE:
			i++;
		case ERR_PARTS_CREATE:
			i++;
		case CMP_TRAITS:
			i++;
		case ERR_TRAITS:
			i++;
		case IN_TRAITS:
			i++;
		case CMP_PAGE_CREATE:
			i++;
		case IN_PAGE_CREATE:
			i++;
		case UNIT_NEW:
			i++;
			return i;
		}
		return Integer.MAX_VALUE;
	}

	public static int getOrderedStatus(int status, int def) {
		int val = getOrderedStatus(status);
		if (val == Integer.MAX_VALUE) {
			return def;
		} else {
			return val;
		}
	}

	/**
	 * ステータスがNX読取完了より後ろの状態かどうか判定します。
	 * @param status
	 * @return
	 */
	public static boolean isReaded(int status) {
		switch (status) {
			case CMP_NX_READ: // NX読取完了（resultがセットされた状態）
			case IN_VERIFY: // エントリー中
			case ERR_VERIFY: // エントリーエラー
			case FIN_VERIFY: // エントリー完了（仮入力）
			case IN_VERIFY2: // ベリファイ中
			case CMP_VERIFY: // ベリファイ完了（承認済）
			case ERR_MANIPULATION: // 加工処理エラー
			case IN_MANIPULATION: // 加工処理中
			case CMP_MANIPULATION: // 加工処理完了
			case ERR_CSV_WRITE: // CSV出力エラー
			case IN_CSV_WRITE: // CSV出力中
			case CMP_CSV_WRITE: // CSV出力完了
				return true;
			default:
				return false;
		}
	}

	/**
	 * ステータスがベリファイ済より後ろの状態かどうか判定します。
	 * @param status
	 * @return
	 */
	public static boolean isVerified(int status) {
		switch (status) {
			case CMP_VERIFY: // ベリファイ完了（承認済）
			case ERR_MANIPULATION: // 加工処理エラー
			case IN_MANIPULATION: // 加工処理中
			case CMP_MANIPULATION: // 加工処理完了
			case ERR_CSV_WRITE: // CSV出力エラー
			case IN_CSV_WRITE: // CSV出力中
			case CMP_CSV_WRITE: // CSV出力完了
				return true;
			default:
				return false;
		}
	}

	/**
	 * ステータスが加工完了以降の状態かどうか判定します。
	 * @param status 状態
	 * @return
	 */
	public static boolean isManipulated(int status) {
		switch (status) {
			case CMP_MANIPULATION: // 加工処理完了
			case ERR_CSV_WRITE: // CSV出力エラー
			case IN_CSV_WRITE: // CSV出力中
			case CMP_CSV_WRITE: // CSV出力完了
				return true;
			default:
				return false;
		}
	}
}
