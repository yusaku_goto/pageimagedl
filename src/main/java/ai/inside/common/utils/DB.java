package ai.inside.common.utils;

import org.apache.commons.dbcp2.BasicDataSourceFactory;

import javax.sql.DataSource;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * コネクションプーリングを考慮したDB接続ユーティリティ
 * @version 1.0.1
 */
public class DB {
	/** 唯一のデータソースオブジェクト */
	private static DataSource ds;
	/** SQL文をコンソール出力するかどうか */
	private static boolean debugSql = "true".equals(Env.getSingleton().getProperty("debug.sql"));

	/**
	 * データソースを返します。
	 * @return データソース
	 * @throws Exception
	 */
	protected static synchronized DataSource getDataSource() throws Exception {
		if (ds == null) {
			Properties properties = new Properties();
			try (InputStream is = DB.class.getResourceAsStream("/dbcp.properties")) {
				properties.load(is);
			}
			ds = BasicDataSourceFactory.createDataSource(properties);
		}
		return ds;
	}

	/**
	 * データソースからデータベース接続を返します。
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {
		try {
			Connection con = getDataSource().getConnection();
			if ("true".equals(Env.getSingleton().getProperty("mysql.useUtf8mb4"))) {
				try (Statement st = con.createStatement()) {
					st.executeQuery("SET NAMES utf8mb4");
				}
			}
			if ("true".equals(Env.getSingleton().getProperty("mysql.isolation.readCommitted"))) {
				try (Statement st = con.createStatement()) {
					st.executeQuery("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED");
				}
			}

			// ログ出力用のラッパーProxy
			if (debugSql) {
				con = (Connection) Proxy.newProxyInstance(
						Connection.class.getClassLoader(),
						new Class[]{Connection.class},
						new ConnectionProxyHandler(con));
			}
			con.setAutoCommit(false);
			return con;
		} catch (Exception e) {
			throw new SQLException("Cannot create DataSource.", e);
		}
	}
}
