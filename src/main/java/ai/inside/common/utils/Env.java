package ai.inside.common.utils;

import ai.inside.common.logging.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * env.propertiesを読み込み設定値を保持します。
 * @author yuukiitonaga
 *
 */
@SuppressWarnings("serial")
public class Env extends Properties {
	private static final Properties prop = new Properties(); // Default properties (original env.properties key-values)
	private static Env instance; // Singleton instance of Env
	static {
		try (InputStream is = Env.class.getResourceAsStream("/env.properties")) {
			prop.load(is);
		} catch (IOException e) {
			Log.error("env.properties ファイルの読込に失敗しました", e);
		}
	}
	Env() {
		super(prop);
	}
	public static Env getSingleton() {
		if (instance == null) {
			instance = new Env();
		}
		return instance;
	}
}
