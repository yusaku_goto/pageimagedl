package ai.inside.common.utils;

import ai.inside.common.logging.Log;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

public class PreparedStatementProxyHandler implements InvocationHandler {
	private final PreparedStatement ps;
	private final String sql;
	private final Map<Integer, String> paramMap = new HashMap<>();
	PreparedStatementProxyHandler(PreparedStatement ps, String sql) {
		this.ps = ps;
		this.sql = sql;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// psへのセッターの呼び出しの場合は配列にセット
		// 全てのセッターメソッドに対応していないので、随時追加してください
		String methodName = method.getName();
		if (methodName.equals("setString")
				|| methodName.equals("setLong")
				|| methodName.equals("setInt")
				|| methodName.equals("setBytes")
				|| methodName.equals("setTimestamp")
				|| methodName.equals("setFloat")
				|| methodName.equals("setDouble")
				) {
			paramMap.put((Integer)args[0], String.valueOf(args[1]));
		}
		// SQL実行の場合のみSQLの監視と処理時間のロギング
		if (methodName.equals("executeQuery")
				|| methodName.equals("executeUpdate")) {
			long start = System.currentTimeMillis();
			try {
				return method.invoke(ps, args);
			} finally {
				long end = System.currentTimeMillis();
				Log.debug("SQL " + (end-start) + "ms: " + getSql());
			}
		} else {
			return method.invoke(ps, args);
		}
	}
	private String getSql() {
		StringBuilder sb = new StringBuilder(sql);
		int i = 0;
		int index = 0;
		while ((index = sb.indexOf("?", index)) != -1) {
			sb.deleteCharAt(index);
			String val = String.valueOf(paramMap.get(++i));
			sb.insert(index, val != null ? ('\'' + val + '\'') : val);
			index += val.length();
		}
		return sb.toString();
	}

}
