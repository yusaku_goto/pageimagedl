package ai.inside.common.logging;

import ai.inside.common.utils.Env;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Java Logger へのブリッジクラスです。
 *
 * このクラスを呼ぶことで個々のクラスにloggerインスタンスを保持する必要がない。
 *
 */
public class Log {

	private static Logger logger = Logger.getLogger("batch");
	private static ErrorMessenger errorMessengerBySlack = new ErrorMessengerSlackImpl();

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		Log.logger = logger;
	}

	private static String errorOutput = Env.getSingleton().getProperty("error.log.output.pattern", "1");

	/**
	 * メッセージをログ出力します。（デバッグレベル）
	 * 前提：呼び出し元で、出力ログメッセージは生成する
	 *
	 * @param msg
	 */
	public static void debug(String msg) {
		log(Level.DEBUG, msg, null);
	}
	/**
	 * メッセージをログ出力します。（インフォレベル）
	 * 前提：呼び出し元で、出力ログメッセージは生成する
	 *
	 * @param msg
	 */
	public static void info(String msg) {
		log(Level.INFO, msg, null);
	}
	/**
	 * メッセージをログ出力します。（ワーニングレベル）
	 * 前提：呼び出し元で、出力ログメッセージは生成する
	 *
	 * @param msg
	 */
	public static void warning(String msg) {
		log(Level.WARN, msg, null);
	}
	/**
	 * メッセージをログ出力します。（エラーレベル）
	 * 前提：呼び出し元で、出力ログメッセージは生成する
	 *
	 * @param msg
	 */
	public static void error(String msg) {
		log(Level.ERROR, msg, null);
		errorMessengerBySlack.send(Level.ERROR, msg);
	}
	/**
	 * メッセージをログ出力します。（エラーレベル：例外時）
	 * 前提：呼び出し元で、出力ログメッセージは生成する
	 *
	 * @param msg
	 * @param e
	 */
	public static void error(String msg, Throwable e) {
		log(Level.ERROR, msg ,e);
		errorMessengerBySlack.send(Level.ERROR, msg, e);
	}

	/**
	 * ログメッセージにログレベルに追記する（必須）
	 * スタックスレースがある場合、ログに追記する（任意、例外時は必須）
	 *
	 * @param level
	 * @param msg
	 * @param e
	 */
	private static void log(Level level, String msg, Throwable e) {
		String logFormat = String.join(" : ", level.toString(), msg);
		logger.log(level, logFormat, e);
	}
}