package ai.inside.common.logging;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Priority;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.InetAddress;
import java.util.Properties;

/**
 * エラーを Slack へ送るクラスです。
 *
 * @author yusaku_g
 */
public class ErrorMessengerSlackImpl implements ErrorMessenger {

	private Client client = ClientBuilder.newClient();

	private String url;
	private String path;
	private boolean enable;
	private String envLabel;
	private String hostName;

	ErrorMessengerSlackImpl() {
		// slack 送信先を取得
		Properties prop = new Properties();
		String envVal = "";    // 実行サーバのラベルを読み込む環境変数
		String defaultEnvLabel = "";
		try (InputStream is = ErrorMessengerSlackImpl.class.getResourceAsStream("/slack.properties")) {
			prop.load(is);
			url = prop.getProperty("url");
			path = prop.getProperty("path");
			envVal = prop.getProperty("slack.env.label.env.variable", "");
			defaultEnvLabel = prop.getProperty("slack.env.label.default", "");
			enable = Boolean.valueOf(prop.getProperty("slack.on"));
		} catch (IOException e) {
			Log.error("Slackの設定が取得できません", e);
		}
		// 実行サーバのラベルを環境変数から取得
		envLabel = getEnvLabel(envVal, defaultEnvLabel);
		// ホスト名を取得
		hostName = getHostName();
	}

	@Override
	public void send(Priority priority, String message) {
		if (!enable) {
			return;
		}
		String env = envLabel + " " + hostName;
		SimpleSlackMessage s = new SimpleSlackMessage();
		s.setText(String.join("\n",
				env + " にて " + priority.toString() + " が発生しました.",
				"メッセージ=" + message
		));
		post(s.toJsonString());
	}

	@Override
	public void send(Priority priority, String message, Throwable e) {
		if (!enable) {
			return;
		}
		String env = envLabel + " " + hostName;
		StackTraceElement element = e.getStackTrace()[0];
		SimpleSlackMessage s = new SimpleSlackMessage();
		s.setText(String.join("\n",
				env + " にて " + priority.toString() + " が発生しました.",
				"クラス=" + element.getClassName(),
				"メソッド=" + element.getMethodName(),
				"行=" + element.getLineNumber(),
				"メッセージ=" + message
		));
		post(s.toJsonString());
	}

	private void post(String params) {
		client.target(url).path(path)
				.request()
				.post(Entity.entity(params, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
	}

	class SimpleSlackMessage {

		private String text;

		public void setText(String text) {
			this.text = text;
		}

		String toJsonString() {
			final StringWriter w = new StringWriter();
			try (final JsonGenerator gen = Json.createGenerator(w)) {
				gen.writeStartObject()
						.write("text", text)
						.writeEnd();
			}
			return w.toString();
		}
	}

	private String getEnvLabel(String env, String defaultEnvLabel) {
		String envLabel = System.getenv(env);
		if (envLabel == null) {
			envLabel = defaultEnvLabel;
		}
		return envLabel;
	}

	private String getHostName() {
		String hostName = "";
		try {
			hostName = InetAddress.getLocalHost().getHostName();
			if (StringUtils.isEmpty(hostName)) {
				hostName = System.getenv("HOSTNAME");
			}
		} catch (Exception e) {
			Log.warning(e.getMessage());
		}
		return hostName;
	}

}
