package ai.inside.common.logging;

import org.apache.log4j.Priority;

public interface ErrorMessenger {

	/**
	 * エラーメッセージの送信
	 *
	 * @param priority エラーレベル
	 * @param message  メッセージ
	 */
	void send(Priority priority, String message);


	/**
	 * エラーメッセージの送信
	 *
	 * @param priority エラーレベル
	 * @param message  メッセージ
	 * @param e        例外
	 */
	void send(Priority priority, String message, Throwable e);

}
