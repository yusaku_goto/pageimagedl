package ai.inside.common.entity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 読取ユニットのエンティティクラスです。
 * @author masayaarai
 *
 */
@SuppressWarnings("serial")
public class ReadingUnit extends BaseEntity implements IEntity, IPermitEntity {
	private int id;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Timestamp deletedAt;
	private int licenseId;
	private int organizationId;
	private int departmentId;
	private int docsetId;
	private int documentId;
	private int userId;
	private int status;
	private String importDir;
	private String exportDir;
	private int verifyUserId;
	private String name;
	private String fileName;
	private int isArchived;
	private String csvFileName;
	private int isCsvLocked;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Timestamp getDeletedAt() {
		return deletedAt;
	}
	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}
	public int getLicenseId() {
		return licenseId;
	}
	public void setLicenseId(int licenseId) {
		this.licenseId = licenseId;
	}
	public int getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getDocsetId() {
		return docsetId;
	}
	public void setDocsetId(int docsetId) {
		this.docsetId = docsetId;
	}
	public int getDocumentId() {
		return documentId;
	}
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getImportDir() {
		return importDir;
	}
	public void setImportDir(String importDir) {
		this.importDir = importDir;
	}
	public String getExportDir() {
		return exportDir;
	}
	public void setExportDir(String exportDir) {
		this.exportDir = exportDir;
	}
	public int getVerifyUserId() {
		return verifyUserId;
	}
	public void setVerifyUserId(int verifyUserId) {
		this.verifyUserId = verifyUserId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getIsArchived() {
		return isArchived;
	}
	public void setIsArchived(int isArchived) {
		this.isArchived = isArchived;
	}
	public String getCsvFileName() {
		return csvFileName;
	}
	public void setCsvFileName(String csvFileName) {
		this.csvFileName = csvFileName;
	}
	public int getIsCsvLocked() {
		return isCsvLocked;
	}
	public void setIsCsvLocked(int isCsvLocked) {
		this.isCsvLocked = isCsvLocked;
	}

	/**
	 * 全て取得
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static List<ReadingUnit> findAll(Connection con, int licenseId) throws SQLException {
		List<ReadingUnit> list = new ArrayList<>();
		String sql = "SELECT id, created_at, updated_at, deleted_at, "
				+ "license_id, organization_id, department_id, "
				+ "docset_id, document_id, user_id, "
				+ "status, import_dir, export_dir, "
				+ "verify_user_id, name, file_name, "
				+ "is_archived, csv_file_name, is_csv_locked "
				+ "FROM reading_units "
				+ "WHERE license_id = ? " +
				"AND deleted_at IS NULL ";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, licenseId);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					ReadingUnit u = new ReadingUnit();
					u.set(rs);
					list.add(u);
				}
			}
		}
		return list;
	}

	/**
	 * 指定したIDより大きいIDの読取ユニットを検索
	 * @param con
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static List<ReadingUnit> findByReadingUnitGreaterThan(Connection con, int id, int licenseId) throws SQLException {
		List<ReadingUnit> list = new ArrayList<>();
		String sql = "SELECT id, created_at, updated_at, deleted_at, "
				+ "license_id, organization_id, department_id, "
				+ "docset_id, document_id, user_id, "
				+ "status, import_dir, export_dir, "
				+ "verify_user_id, name, file_name, "
				+ "is_archived, csv_file_name, is_csv_locked "
				+ "FROM reading_units "
				+ "WHERE id > ? AND license_id = ? AND deleted_at IS NULL";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, id);
			ps.setInt(2, licenseId);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					ReadingUnit u = new ReadingUnit();
					u.set(rs);
					list.add(u);
				}
			}
		}
		return list;
	}

	/**
	 * ResultSetからフィールドに値をセットします。
	 * @param rs
	 * @throws SQLException
	 */
	protected void set(ResultSet rs) throws SQLException {
		int i = 0;
		setId(rs.getInt(++i));
		setCreatedAt(rs.getTimestamp(++i));
		setUpdatedAt(rs.getTimestamp(++i));
		setDeletedAt(rs.getTimestamp(++i));
		setLicenseId(rs.getInt(++i));
		setOrganizationId(rs.getInt(++i));
		setDepartmentId(rs.getInt(++i));
		setDocsetId(rs.getInt(++i));
		setDocumentId(rs.getInt(++i));
		setUserId(rs.getInt(++i));
		setStatus(rs.getInt(++i));
		setImportDir(rs.getString(++i));
		setExportDir(rs.getString(++i));
		setVerifyUserId(rs.getInt(++i));
		setName(rs.getString(++i));
		setFileName(rs.getString(++i));
		setIsArchived(rs.getInt(++i));
		setCsvFileName(rs.getString(++i));
		setIsCsvLocked(rs.getInt(++i));
	}
}
