package ai.inside.common.entity;

import java.sql.Timestamp;
import java.util.List;

/**
 * データベースエンティティの共通インターフェースです。
 * @author yuukiitonaga
 *
 */
public interface IEntity {

	int getId();

	Timestamp getCreatedAt();

	Timestamp getUpdatedAt();

	Timestamp getDeletedAt();

	/**
	 * リストの中で id 値が一致する最初のオブジェクトを返します。
	 * @param <T> IEntity のサブクラス
	 * @param list 探索を行うリスト
	 * @param id 探索するid
	 * @return idが一致する最初のオブジェクト、見つからない場合は null を返します。
	 * @throws NullPointerException list が null の場合
	 */
	public static <T extends IEntity> T findInList(List<T> list, int id) throws NullPointerException {
		for (T e : list)
			if (e != null && e.getId() == id)
				return e;
		return null;
	}
	
	/**
	 * リストの中で id 値が一致する最初のオブジェクトを返します。このメソッドは idが非ゼロかどうかのチェックを含めています。
	 * @param <T> IEntity のサブクラス
	 * @param list 探索を行うリスト
	 * @param id 探索するid
	 * @param ignoreZero このフラグがtrueかつ、id==0の場合は探索を行わずnullを返します。
	 * @return idが一致する最初のオブジェクト、見つからない場合は null を返します。
	 * @throws NullPointerException list が null の場合
	 */
	public static <T extends IEntity> T findInList(List<T> list, int id, boolean ignoreZero) throws NullPointerException {
		if (!ignoreZero || id != 0)
			return findInList(list, id);
		return null;
	}
	
}
