package ai.inside.common.entity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 読取ページのエンティティクラスです。
 * @author masayaarai
 *
 */
@SuppressWarnings("serial")
public class ReadingPage extends BaseEntity implements IEntity, IPermitEntity {
	private int id;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Timestamp deletedAt;
	private int licenseId;
	private int organizationId;
	private int departmentId;
	private int userId;
	private int docsetId;
	private int documentId;
	private int readingUnitId;
	private int status;
	private String fileName;
	private byte[] image;
	private String extraData;
	private Timestamp verifyUpdatedAt;
	private Timestamp readDoneAt;
	private int width;
	private int height;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Timestamp getDeletedAt() {
		return deletedAt;
	}
	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}
	public int getLicenseId() {
		return licenseId;
	}
	public void setLicenseId(int licenseId) {
		this.licenseId = licenseId;
	}
	public int getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getDocsetId() {
		return docsetId;
	}
	public void setDocsetId(int docsetId) {
		this.docsetId = docsetId;
	}
	public int getDocumentId() {
		return documentId;
	}
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
	public int getReadingUnitId() {
		return readingUnitId;
	}
	public void setReadingUnitId(int readingUnitId) {
		this.readingUnitId = readingUnitId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getExtraData() {
		return extraData;
	}
	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}
	public Timestamp getVerifyUpdatedAt() {
		return verifyUpdatedAt;
	}
	public void setVerifyUpdatedAt(Timestamp verifyUpdatedAt) {
		this.verifyUpdatedAt = verifyUpdatedAt;
	}
	public Timestamp getReadDoneAt() {
		return readDoneAt;
	}
	public void setReadDoneAt(Timestamp readDoneAt) {
		this.readDoneAt = readDoneAt;
	}

	/**
	 * 読取ユニットIDからの検索。
	 * @param con データベース接続子
	 * @param readingUnitId 読取ページID
	 * @param image image列を取得するかどうか
	 * @return
	 * @throws SQLException
	 */
	public static List<ReadingPage> findByReadingUnitId(Connection con, int readingUnitId, boolean image) throws SQLException {
		List<ReadingPage> list = new ArrayList<>();
		String sql = "SELECT id, created_at, updated_at, deleted_at, "
				+ "license_id, organization_id, department_id, docset_id, "
				+ "document_id, user_id, reading_unit_id, "
				+ "status, "
				+ "file_name" + (image ? ", image" : "") + ", extra_data, "
				+ "verify_updated_at, read_done_at, "
				+ "width, height "
				+ "FROM reading_pages "
				+ "WHERE reading_unit_id=? "
				+ "AND deleted_at IS NULL "
				+ "ORDER BY id";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			int i = 0;
			ps.setInt(++i, readingUnitId);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					ReadingPage u = new ReadingPage();
					u.set(rs, image);
					list.add(u);
				}
			}
			return list;
		}
	}

	/**
	 * 読取ユニットIDからの検索。
	 * @param con
	 * @param readingUnitId
	 * @return
	 * @throws SQLException
	 */
	public static List<Integer> findIdByReadingUnitId(Connection con, int readingUnitId) throws SQLException, ReflectiveOperationException {
		List<Integer> list = new ArrayList<>();
		String sql = "SELECT id, created_at, updated_at, deleted_at, "
				+ "license_id, organization_id, department_id, docset_id, "
				+ "document_id, user_id, reading_unit_id, "
				+ "status, "
				+ "file_name, image, extra_data, "
				+ "verify_updated_at, read_done_at, "
				+ "width, height "
				+ "FROM reading_pages "
				+ "WHERE reading_unit_id=? "
				+ "AND deleted_at IS NULL "
				+ "ORDER BY id";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			int i = 0;
			ps.setInt(++i, readingUnitId);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					list.add(rs.getInt(1));
				}
			}
			return list;
		}
	}

	/**
	 * ResultSetからフィールドに値をセットします。
	 * @param rs
	 * @param image image列を取得するかどうか
	 * @throws SQLException
	 */
	protected void set(ResultSet rs, boolean image) throws SQLException {
		int i = 0;
		setId(rs.getInt(++i));
		setCreatedAt(rs.getTimestamp(++i));
		setUpdatedAt(rs.getTimestamp(++i));
		setDeletedAt(rs.getTimestamp(++i));
		setLicenseId(rs.getInt(++i));
		setOrganizationId(rs.getInt(++i));
		setDepartmentId(rs.getInt(++i));
		setDocsetId(rs.getInt(++i));
		setDocumentId(rs.getInt(++i));
		setUserId(rs.getInt(++i));
		setReadingUnitId(rs.getInt(++i));
		setStatus(rs.getInt(++i));
		setFileName(rs.getString(++i));
		if (image) {
			setImage(rs.getBytes(++i));
		}
		setExtraData(rs.getString(++i));
		setVerifyUpdatedAt(rs.getTimestamp(++i));
		setReadDoneAt(rs.getTimestamp(++i));
		setWidth(rs.getInt(++i));
		setHeight(rs.getInt(++i));
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
}
