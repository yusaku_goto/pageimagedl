package ai.inside.common.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * データベースの共通エンティティです。
 * @author bungo ba
 *
 */
@SuppressWarnings("serial")
public class BaseEntity implements Serializable {

	/**
	 * テーブル名で同じdocumentIdのデータを削除
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static int deleteByDocumentId(Connection con,int documentId ,String tableName) throws SQLException {
		String sql = "DELETE FROM " + tableName
				+ " WHERE document_id=? ";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, documentId);
			return ps.executeUpdate();
		}
	}

	/**
	 * テーブル名で同じdocsetIdのデータを削除
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static int deleteByDocsetId(Connection con,int docsetId ,String tableName) throws SQLException {
		String sql = "DELETE FROM " + tableName
				+ " WHERE docset_id=? ";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, docsetId);
			return ps.executeUpdate();
		}
	}

	/**
	 * テーブル名で同じdepartmentIdのデータを削除
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static void deleteByDepartmentId(Connection con,int departmentId ,String tableName) throws SQLException {
		String sql = "DELETE FROM " + tableName
				+ " WHERE department_id=? ";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, departmentId);
			ps.executeUpdate();
		}
	}

	/**
	 * テーブル名で同じorganizationIdのデータを削除
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static int deleteByOrganizationId(Connection con,int organizationId ,String tableName) throws SQLException {
		String sql = "DELETE FROM " + tableName
				+ " WHERE organization_id=? ";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, organizationId);
			return ps.executeUpdate();
		}
	}

	/**
	 * テーブル名で同じ請求先IDのデータを削除
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static int deleteByLicenseId(Connection con,int licenseId,String tableName) throws SQLException {
		String sql = "DELETE FROM " + tableName
				+ " WHERE license_id=? ";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, licenseId);
			return ps.executeUpdate();
		}
	}

	/**
	 * SQL文のIN句に使えるパラメータ文字列を生成します。
	 * @param length ?記号の発生回数
	 * @return
	 */
	protected static String getInParam(int length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			sb.append(i == 0 ? "?" : ",?");
		}
		return sb.toString();
	}

	/**
	 * DB別タイムスタンプを生成する。
	 *
	 * @return
	 */
	protected static Timestamp generateCurrentTimestamp() {
		return new Timestamp(new BigDecimal(System.currentTimeMillis()).setScale(-3, BigDecimal.ROUND_HALF_DOWN).longValue());
	}
}
