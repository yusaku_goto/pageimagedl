package ai.inside.common.entity;

/**
 * このデータベースエンティティにはライセンス、組織、部署のフィールドがあり、
 * 共通の権限チェック(Utils.checkPermission)が行えます。
 */
public interface IPermitEntity {

	int getLicenseId();

	int getOrganizationId();

	int getDepartmentId();

}