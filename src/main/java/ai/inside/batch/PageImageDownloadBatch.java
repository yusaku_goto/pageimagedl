package ai.inside.batch;

import ai.inside.common.entity.ReadingPage;
import ai.inside.common.entity.ReadingUnit;
import ai.inside.common.logging.Log;
import ai.inside.common.utils.DB;
import ai.inside.common.utils.Env;
import ai.inside.common.utils.ReadingStatus;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageImageDownloadBatch {

	private static SimpleDateFormat dateFormatter;
	private static String memoFileName = "last_id_memo.txt";
	private static String downloadPath;
	private static int executingMinute = -1;
	private static int licenseId;
	static {
		downloadPath = Env.getSingleton().getProperty("download.path");

		String tmpStr = Env.getSingleton().getProperty("execute.limit.minute");
		if (!StringUtils.isBlank(tmpStr)) {
			executingMinute = Integer.parseInt(tmpStr);
		}

		String timeFormat = "yyyyMMdd_HHmmss";
		dateFormatter = new SimpleDateFormat(timeFormat);

//		licenseId = 153;    // ステージングよう
		licenseId = 221;    // 本番用 (インサイトさま)
	}

	public static void main(String... args) {
		if (StringUtils.isBlank(downloadPath)) {
			Log.error("画像の保存先ディレクトリが設定されていません");
			return;
		}
		if (!Files.exists(Paths.get(downloadPath))) {
			try {
				Files.createDirectory(Paths.get(downloadPath));
			} catch (IOException e) {
				Log.error("画像保存先ディレクトリの作成に失敗しました", e);
				return;
			}
		}

		// ロックファイルによる多重起動制御
		File lockFile = new File("PageImageDownload.lock");
		try (FileOutputStream fos = new FileOutputStream(lockFile);
		     FileChannel fc = fos.getChannel();
		     FileLock lock = fc.tryLock()) {
			if (lock == null) {
				Log.info("他のプロセスが起動中です");
			} else {
				Log.info(MessageFormat.format("ロックファイルは{0}です", lockFile.getAbsolutePath()));
				run();
				lockFile.deleteOnExit();
			}
		} catch (IOException e) {
			Log.error("起動ロックファイルの獲得で例外が発生しました", e);
		}
	}

	/**
	 * ページ画像取得処理
	 */
	private static void run() {
		try (Connection con = DB.getConnection()) {
			Log.info("ページ画像取得処理を開始します");
			if (executingMinute != -1) Log.info(MessageFormat.format("実行時間制限は{0}分です", executingMinute));
			long start = System.currentTimeMillis();

			// 最後に画像を取得したReadingUnitIdを取得する
			int lastReadingUnitId = fetchLastDownloadReadingUnitId();

			// ユニットを取得
			// ※ ページを直接取得すると、削除済のユニットに紐付くページも取得してしまう為
			List<ReadingUnit> readingUnitList;
			if (lastReadingUnitId == -1) {
				// 全部取得
				readingUnitList = ReadingUnit.findAll(con, licenseId);
			} else {
				// lastReadingUnitIdより大きいIDのみ取得
				readingUnitList = ReadingUnit.findByReadingUnitGreaterThan(con, lastReadingUnitId, licenseId);
			}
			if (readingUnitList.size() == 0) {
				Log.warning("対象の読取ユニットが存在しません");
				Log.info("ページ画像取得処理を終了します");
				return;
			}
			// ユニットに紐付くページから画像情報を取得、保存
			String nextLastReadingUnitId = "";  // 次回起動時用の最後に画像を取得したReadingUnitId
			Map<String, Integer> fileCountMap = new HashMap<>();
			for (ReadingUnit unit : readingUnitList) {
				if (unit.getStatus() < ReadingStatus.CMP_NX_READ &&
						unit.getCreatedAt().getTime() > System.currentTimeMillis() - (1000 * 60 * 5)) {
					Log.info("ページ取り込み途中の読取ユニットに当たりました。処理を中断します。");
					break;
				}
				int readingUnitId = unit.getId();
				String downloadDirPath = "";
				if (unit.getDocsetId() == 1400) {
					downloadDirPath = "credence";
				} else if (unit.getDocsetId() == 870) {
					downloadDirPath = "insite";
				} else {
					downloadDirPath = "others";
				}
				Log.info("対象ユニット: " + readingUnitId);
				List<ReadingPage> readingPageList = ReadingPage.findByReadingUnitId(con, readingUnitId, true);
				String dirName = "";
				if (readingPageList.size() > 0) {
					Log.info("対象ページ数: " + readingPageList.size());
					String unitName = StringUtils.isBlank(unit.getName()) ? "" : unit.getName();
					unitName = unitName.replaceAll("\\s", "_");
//					dirName = MessageFormat.format("{0}/{1}_{2}", downloadPath, unitName, dateFormatter.format(unit.getCreatedAt()));
					dirName = downloadPath + "/" + downloadDirPath;
				}
				for (ReadingPage page : readingPageList) {
					Log.info("対象ページ: " + page.getId());
					// png か jpg か確認
					String format = null;
					format = isPNG(page.getImage()) ? ".png" : format;
					format = isJPG(page.getImage()) ? ".jpg" : format;
					// 画像が対応しているものかチェック
					if (format == null) {
						Log.warning(MessageFormat.format("ReadingPageId: {0} の画像は非対応フォーマットです. png, jpg に対応しています", page.getId()));
						continue;
					}
					// ディレクトリの存在チェック
					if (!Files.exists(Paths.get(dirName))) {
						Files.createDirectory(Paths.get(dirName));
					}
					String fileName = page.getFileName();
					fileName = fileName.replace(".pdf", "");
					fileName = fileName.replace(".png", "");
					fileName = fileName.replace(".jpg", "");
					fileName = fileName.replace(".jpeg", "");
					fileName = fileName.replace(".tiff", "");
					if (Files.exists(Paths.get(dirName + "/" + fileName + format)) || Files.exists(Paths.get(dirName + "/" + fileName + "_1" + format))) {
						Log.info("ファイル名が重複しました: " + fileName + format);
						// ファイル名が重複しているので連番を振る
						Log.info(fileCountMap.keySet().toString());
						if (fileCountMap.containsKey(fileName)) {
							Log.info("既存のファイル重複");
							// あるなら +1 を降る
							int num = fileCountMap.get(fileName) + 1;
							fileCountMap.put(fileName, num);
							fileName += ("_" + num);
						} else {
							Log.info("初回のファイル名重複");
							// ないなら既存ファイルに _1 を振る
							if (Files.exists(Paths.get(dirName + "/" + fileName + format))) {
								Path existingFilePath = Paths.get(dirName + "/" + fileName + format);
								Path targetFilePath = Paths.get(dirName + "/" + fileName + "_1" + format);
								Files.move(existingFilePath, targetFilePath);
							}
							// そして今回のファイルに _2 を振る
							fileCountMap.put(fileName, 2);
							fileName += ("_" + 2);
						}
					}
					// 画像を保存
					if(!imageSave(page.getImage(), dirName, fileName + format)) {
						Log.error("画像の保存に失敗しました。 ReadingPageId: " + page.getId());
						writeLastReadingUnitId(nextLastReadingUnitId);
						Log.info("ページ画像取得処理を終了します");
						return;
					}
				}
				nextLastReadingUnitId = String.valueOf(readingUnitId);

				if (executingMinute == -1) continue;    // 実行時間制限の設定がなければ飛ばす
				long now = System.currentTimeMillis();
				if ((now - start) > (1000 * 60 * executingMinute)) {
					Log.info("実行制限時間を超過しましたので処理を終了します");
					break;
				}
			}
			if (!StringUtils.isBlank(nextLastReadingUnitId)) {
				writeLastReadingUnitId(nextLastReadingUnitId);
			}
			Log.info("ページ画像取得処理を終了します");
		} catch (Exception e) {
			Log.error("画像取得・保存バッチ処理途中でエラーが発生しました", e);
		}
	}

	/**
	 * 画像を保存する
	 *
	 * @param image 保存対象の画像情報
	 * @param dirName 保存先のディレクトリ名 (フルパス)
	 * @param imageFileName 保存する画像ファイル名
	 * @return 成否
	 */
	private static boolean imageSave(byte[] image, String dirName, String imageFileName/*, String format*/) {
		try {
			File imgFile = new File(dirName + "/" + imageFileName);
//			FileImageOutputStream out = new FileImageOutputStream(imgFile);
//			BufferedImage bi = ImageIO.read(new ByteArrayInputStream(image));
//			ImageIO.write(new ByteArrayInputStream(image));
			Files.write(imgFile.toPath(), image);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 最後に保存された ReadingUnitId をメモへ保存する
	 * ファイルがない場合は新規作成する
	 *
	 * @param lastReadingUnitId 最後に保存された ReadingUnitId
	 */
	private static void writeLastReadingUnitId(String lastReadingUnitId) throws IOException {
		Path memoFilePath = Paths.get(memoFileName);
		if (!Files.exists(memoFilePath)) {
			Files.createFile(memoFilePath);
		}
		Files.write(memoFilePath, lastReadingUnitId.getBytes(StandardCharsets.UTF_8));
	}

	/**
	 * PNGか確認
	 *
	 * @param image 画像のバイト配列
	 * @return PNG なら true
	 */
	private static boolean isPNG(byte[] image) {
		return "89504E470D0A1A0A".equals(getByteHeaderString(image));
	}

	/**
	 * JPGか確認
	 *
	 * @param image 画像のバイト配列
	 * @return JPG なら true
	 */
	private static boolean isJPG(byte[] image) {
		return getByteHeaderString(image).indexOf("FFD8") == 0;
	}

	/**
	 * バイト配列の先頭8文字を16進数で取得
	 *
	 * @param bytes 対象のバイト配列
	 * @return 先頭8文字を大文字16進数の文字列に変換したもの
	 */
	private static String getByteHeaderString(byte[] bytes) {
		byte[] header = new byte[8];
		System.arraycopy(bytes, 0, header, 0, header.length);
		StringBuilder sb = new StringBuilder();
		for (byte h : header) {
			sb.append(String.format("%02X", h));
		}
		return sb.toString();
	}

	/**
	 * 最後に取得したReadingUnit.id を取得する
	 */
	private static int fetchLastDownloadReadingUnitId() {
		List<String> lines = new ArrayList<>();
		try {
			Path memoFilePaths = Paths.get(memoFileName);
			if (!Files.exists(memoFilePaths)) {
				return -1;
			}
			lines = Files.readAllLines(memoFilePaths);
		} catch (IOException e) {
			Log.error("最後に取得したReadingUnitIdのメモ読取に失敗しました", e);
			e.printStackTrace();
		}
		if (lines.isEmpty()) return -1;

		return Integer.parseInt(lines.get(0));
	}
}
